package com.mumo.wordlookup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordViewHolder> {


    private List<String> words;
    private Context mContext;
    public WordAdapter(Context context){
        mContext = context;
        words = new ArrayList<>();
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
       View view = LayoutInflater.from(mContext).inflate(R.layout.word_list_item, viewGroup, false);
        return new WordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        holder.textView.setText(words.get(position));
    }

    @Override
    public int getItemCount() {
        return words.size();
    }

    public void setWords(List<String> words){
        this.words = words;
        notifyDataSetChanged();
    }

    public class WordViewHolder extends RecyclerView.ViewHolder{

        TextView textView;
        public WordViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_word);
        }
    }
}
